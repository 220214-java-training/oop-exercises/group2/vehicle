public class VarriableDeclaration {
    public static void main (String[] Args ) {
        boolean on = false ;

        System.out.println(on);

        short s = 16;
        System.out.println(s);

        int i = -32;
        System.out.println(i);

        float f = 3839.34839f ;
        System.out.println(f);

        long l = 4294967296l;
        System.out.println(l);

        char c = 'a';
        System.out.println(c);

        System.out.println ("works out well!");

    }
}
